use crate::model::WebhookPush;
use fs_extra::{copy_items, dir::CopyOptions};
use git_ops::update_the_repo;
use log::{error, info};
use std::{
    fs::{read_dir, remove_dir_all, remove_file},
    path::{Path, PathBuf},
    process::Command,
};

use thiserror::Error;
mod git_ops;

#[derive(Error, Debug)]
pub enum PushError {
    #[error("Hugo binary could not run updates")]
    BadHugo,
    #[error("Repo no longer valid repo")]
    InvalidGitRepo,
    #[error("Could not copy updated site to site folder")]
    UnableToUpdateSite,
    #[error("Could not fetch latest")]
    ErrorPullingLatest,
}

#[derive(Error, Debug)]
pub enum SimplePushError {
    #[error("Could not fetch latest")]
    ErrorPullingLatest,
}

pub fn handle_simple_push(
    webhook_push: &WebhookPush,
    blog_location: &PathBuf,
) -> Result<(), SimplePushError> {
    info!(
        "Received push {} - starting update",
        webhook_push
            .commits
            .last()
            .unwrap_or(&Default::default())
            .id
    );
    if update_the_repo(blog_location).is_err() {
        return Err(SimplePushError::ErrorPullingLatest);
    }

    Ok(())
}

pub fn handle_push(
    webhook_push: &WebhookPush,
    hugo_location: &PathBuf,
    blog_location: &PathBuf,
    blog_destination: &PathBuf,
) -> Result<(), PushError> {
    info!(
        "Received push {} - starting update",
        webhook_push
            .commits
            .last()
            .unwrap_or(&Default::default())
            .id
    );

    if update_the_repo(blog_location).is_err() {
        return Err(PushError::ErrorPullingLatest);
    }

    delete_old_public(blog_location);

    info!("Running hugo on blog");
    if Command::new(hugo_location)
        .arg("--debug")
        .arg("--gc")
        .arg("--minify")
        .current_dir(blog_location)
        .status()
        .is_err()
    {
        return Err(PushError::BadHugo);
    }

    delete_old_destination(blog_destination);

    let public = blog_location.join("public");
    info!(
        "Copying blog from: {} to: {}",
        public.to_string_lossy(),
        blog_destination.to_string_lossy()
    );

    let mut co = CopyOptions::new();
    co.overwrite = true;
    co.copy_inside = true;

    if copy_items(&[public], blog_destination, &co).is_err() {
        return Err(PushError::UnableToUpdateSite);
    }

    Ok(())
}

fn delete_old_public(blog_location: &PathBuf) {
    let public = blog_location.join("public");
    info!(
        "Deleting old blog location at: {}",
        public.to_string_lossy()
    );
    delete_all_inside_folder(&public);
}

fn delete_all_inside_folder(location: &PathBuf) {
    let items = read_dir(location);
    if items.is_err() {
        error!("Trouble reading items in directory when trying to delete. Continuing anyway");
        return;
    }

    for entry in items.unwrap() {
        match entry {
            Ok(e) => {
                easy_delete(&e.path());
            }

            Err(e) => error!(
                "Trouble deleting path: {:?}, continuing anyway, error: {}",
                location, e
            ),
        }
    }
}

fn easy_delete(path: &Path) {
    if path.is_file() {
        match remove_file(path) {
            Err(e) => error!("Trouble deleting old file {} error: {}", path.display(), e),
            Ok(_) => (),
        }
    } else {
        match remove_dir_all(path) {
            Err(e) => error!("Trouble deleting old file {} error: {}", path.display(), e),
            Ok(_) => (),
        }
    }
}

fn delete_old_destination(blog_destination: &PathBuf) {
    info!(
        "Deleting old blog destination at: {}",
        blog_destination.to_string_lossy()
    );
    match remove_dir_all(blog_destination) {
        Ok(_) => (),
        Err(e) => error!(
            "Trouble deleting old destination folder: {}... continuing anyway.",
            e
        ),
    }
}
