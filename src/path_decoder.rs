use log::{debug, error};
use serde::Deserialize;
use std::path::{PathBuf, MAIN_SEPARATOR};

static MAX_PATH_DECODE_LENGTH: usize = 1024;

pub enum DecodeError {
    InvalidPath,
    PathDoesNotExist,
    InvalidConfig,
    RequestTooLarge,
}

pub enum SiteConfig {
    SimpleSite(SimpleSiteData),
    HugoSite(HugoSiteData),
}

pub struct SimpleSiteData {
    pub site_path: PathBuf,
}

pub struct HugoSiteData {
    pub unprocessed_blog_location: PathBuf,
    pub processed_blog_destination: PathBuf,
}

#[derive(Deserialize)]
struct RawSiteConfig {
    pub simple_site: Option<RawSimpleSite>,
    pub hugo_site: Option<RawHugoSite>,
}

#[derive(Deserialize)]
struct RawSimpleSite {
    site_path: String,
}

#[derive(Deserialize)]
struct RawHugoSite {
    unprocessed_site_path: String,
    processed_site_path: String,
}

pub fn get_info_for_webhook(maybe_path: &str) -> Result<SiteConfig, DecodeError> {
    let config_dir = decode_dash_param(maybe_path)?;
    get_processed_config(config_dir)
}

fn get_processed_config(config_dir: PathBuf) -> Result<SiteConfig, DecodeError> {
    let config = get_raw_config(config_dir)?;

    if config.simple_site.is_none() && config.hugo_site.is_none() {
        error!("Site must be classified as simple or hugo!");
        Err(DecodeError::InvalidConfig)
    } else {
        validate_config_options(config)
    }
}

fn validate_config_options(config: RawSiteConfig) -> Result<SiteConfig, DecodeError> {
    if config.simple_site.is_some() {
        validate_simple_config(config.simple_site.unwrap())
    } else {
        validate_hugo_config(config.hugo_site.unwrap())
    }
}

fn validate_simple_config(simple_site: RawSimpleSite) -> Result<SiteConfig, DecodeError> {
    let site_path = PathBuf::from(simple_site.site_path);
    if !site_path.exists() {
        error!("Simple site path {:?} doesn't exist. Failing.", site_path);
        Err(DecodeError::PathDoesNotExist)
    } else if !site_path.is_dir() {
        error!(
            "Simple site path {:?} isn't a directory. Failing.",
            site_path
        );
        Err(DecodeError::PathDoesNotExist)
    } else {
        Ok(SiteConfig::SimpleSite(SimpleSiteData { site_path }))
    }
}

fn validate_hugo_config(hugo_site: RawHugoSite) -> Result<SiteConfig, DecodeError> {
    let unprocessed_site_path = PathBuf::from(hugo_site.unprocessed_site_path);
    let processed_site_path = PathBuf::from(hugo_site.processed_site_path);
    validate_hugo_path(&unprocessed_site_path, "unprocessed")?;
    validate_hugo_path(&processed_site_path, "processed")?;

    Ok(SiteConfig::HugoSite(HugoSiteData {
        unprocessed_blog_location: unprocessed_site_path,
        processed_blog_destination: processed_site_path,
    }))
}

fn validate_hugo_path(path: &PathBuf, log_prefix: &str) -> Result<(), DecodeError> {
    if !path.exists() {
        error!(
            "Hugo site {} path {:?} doesn't exist. Failing.",
            log_prefix, path
        );
        Err(DecodeError::PathDoesNotExist)
    } else if !path.is_dir() {
        error!("Hugo path {:?} isn't a directory. Failing.", path);
        Err(DecodeError::PathDoesNotExist)
    } else {
        Ok(())
    }
}

fn get_raw_config(mut config_dir: PathBuf) -> Result<RawSiteConfig, DecodeError> {
    config_dir.push("config.toml");
    if !config_dir.exists() {
        error!("config file {:?} does not exist. Failing.", config_dir);
        Err(DecodeError::PathDoesNotExist)
    } else if !config_dir.is_file() {
        error!("config file {:?} is not a file. Failing.", config_dir);
        Err(DecodeError::InvalidPath)
    } else if let Ok(config_str) = std::fs::read_to_string(&config_dir) {
        let raw_config: RawSiteConfig = toml::from_str(&config_str).map_err(|e| {
            error!("Invalid config file! {}", e);
            DecodeError::InvalidConfig
        })?;

        Ok(raw_config)
    } else {
        error!("Trouble reading in config file {:?} - Failing", config_dir);
        Err(DecodeError::InvalidConfig)
    }
}

fn decode_dash_param(maybe_path: &str) -> Result<PathBuf, DecodeError> {
    // we always want to limit wherever we accept user input
    if maybe_path.len() > MAX_PATH_DECODE_LENGTH {
        error!(
            "Site path too long! Max path length is: {}",
            MAX_PATH_DECODE_LENGTH
        );
        return Err(DecodeError::RequestTooLarge);
    }

    let split = maybe_path.split('-');
    let mut path = PathBuf::new();
    path.push(MAIN_SEPARATOR.to_string());
    split.for_each(|item| path.push(item));
    if !path.exists() {
        error!("Path {:?} does not exist. Failing.", path);
        Err(DecodeError::PathDoesNotExist)
    } else if !path.is_dir() {
        error!("Path {:?} must be a directory. Failing.", path);
        Err(DecodeError::InvalidPath)
    } else {
        debug!("Found path {:?}", path);
        Ok(path)
    }
}
