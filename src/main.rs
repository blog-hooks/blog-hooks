use actix_web::{post, web, App, HttpResponse, HttpServer, Responder};
use env_logger::init;
use lazy_static::{initialize, lazy_static};
use log::{error, info};
use model::{Msg, WebhookPush};
use path_decoder::{get_info_for_webhook, DecodeError, SiteConfig};
use std::{collections::HashMap, path::PathBuf};
use webhook_trigger::{handle_push, handle_simple_push, PushError, SimplePushError};

mod clap_helper;
pub mod model;
pub mod path_decoder;
mod webhook_trigger;

static STARTING_ADDR: &str = "127.0.0.1:4567";

lazy_static! {
    static ref INIT_MAP: HashMap<&'static str, Option<PathBuf>> = {
        let mut init_map = HashMap::new();
        init_map.insert("hugo", clap_helper::clap_up());

        init_map
    };

    #[derive(Debug)]
    static ref HUGO_LOCATION: Option<PathBuf> = INIT_MAP["hugo"].clone();
}

#[post("/site-webhook/{dash_path}")]
async fn site_webhook(
    dash_path: web::Path<String>,
    webhook_push: web::Json<WebhookPush>,
) -> impl Responder {
    if HUGO_LOCATION.is_none() {
        return HttpResponse::InternalServerError().json(Msg {
            msg: "Attempted to update hugo site without hugo properly initialized! Ensure server is initialized with a valid hugo binary.".to_owned()
        });
    }

    match get_info_for_webhook(&dash_path) {
        Ok(config) => match config {
            SiteConfig::HugoSite(site_data) => handle_hugo_result(
                handle_push(
                    &webhook_push,
                    &HUGO_LOCATION.clone().unwrap(),
                    &site_data.unprocessed_blog_location,
                    &site_data.processed_blog_destination,
                ),
                &webhook_push,
            ),
            SiteConfig::SimpleSite(site_data) => {
                handle_simple_result(handle_simple_push(&webhook_push, &site_data.site_path))
            }
        },
        Err(e) => match e {
            DecodeError::InvalidPath | DecodeError::PathDoesNotExist => HttpResponse::NotFound()
                .json(Msg {
                    msg: "Couldn't find config details!".to_owned(),
                }),
            DecodeError::InvalidConfig => HttpResponse::UnprocessableEntity().json(Msg {
                msg: "Couldn't process config file!".to_owned(),
            }),
            DecodeError::RequestTooLarge => HttpResponse::Forbidden().json(Msg {
                msg: "Site path too long.".to_owned(),
            }),
        },
    }
}

fn handle_hugo_result(
    hugo_result: Result<(), PushError>,
    webhook_push: &WebhookPush,
) -> HttpResponse {
    return match hugo_result {
        Err(e) => {
            error!("Failed to process update: {}", e);
            match e {
                PushError::BadHugo => HttpResponse::Gone().json(Msg {
                    msg: format!("Hugo binary is gone: {}", e),
                }),
                PushError::InvalidGitRepo => HttpResponse::PreconditionFailed().json(Msg {
                    msg: format!("Given invalid git repo: {}", e),
                }),
                PushError::UnableToUpdateSite => HttpResponse::ExpectationFailed().json(Msg {
                    msg: format!("Unable to update site: {}", e),
                }),
                PushError::ErrorPullingLatest => HttpResponse::UnprocessableEntity().json(Msg {
                    msg: format!("Failed to pull latest site: {}", e),
                }),
            }
        }
        _ => {
            info!(
                "Successfully updated site from commits: {}",
                webhook_push
                    .commits
                    .iter()
                    .map(|commit| commit.id.clone())
                    .collect::<Vec<String>>()
                    .join(", ")
            );
            return HttpResponse::Ok().json(Msg {
                msg: "OK".to_owned(),
            });
        }
    };
}

fn handle_simple_result(simple_site_result: Result<(), SimplePushError>) -> HttpResponse {
    match simple_site_result {
        Err(e) => match e {
            SimplePushError::ErrorPullingLatest => {
                error!("Failed to process update: {}", e);
                HttpResponse::UnprocessableEntity().json(Msg {
                    msg: format!("Failed to pull latest site: {}", e),
                })
            }
        },
        Ok(_) => HttpResponse::Ok().json(Msg {
            msg: "OK".to_owned(),
        }),
    }
}

#[actix_rt::main]
async fn main() {
    init();
    initialize(&INIT_MAP);
    info!("Starting up webserver on: {}", STARTING_ADDR);
    HttpServer::new(|| App::new().service(site_webhook))
        .workers(1) // Since we don't use a jobserver, we want to ensure we only work on 1 request at a time
        .bind(STARTING_ADDR)
        .expect("Could not bind address")
        .run()
        .await
        .expect("Could not start server");
}
