use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug, Default)]
pub struct Msg {
    pub msg: String,
}

#[derive(Serialize, Deserialize, Debug, Default)]
pub struct WebhookPush {
    pub object_kind: String,
    pub before: String,
    pub after: String,
    pub r#ref: String,
    pub checkout_sha: String,
    pub user_id: u64,
    pub user_username: String,
    pub user_email: String,
    pub user_avatar: String,
    pub project_id: u64,
    pub project: Project,
    pub repository: Repository,
    pub total_commits_count: u64,
    pub commits: Vec<Commit>,
}

#[derive(Serialize, Deserialize, Debug, Default)]
pub struct Project {
    pub id: u64,
    pub name: String,
    pub description: String,
    pub web_url: String,
    pub avatar_url: Option<String>,
    pub git_ssh_url: String,
    pub git_http_url: String,
    pub namespace: String,
    pub visibility_level: u64,
    pub path_with_namespace: String,
    pub default_branch: String,
    pub homepage: String,
    pub url: String,
    pub ssh_url: String,
    pub http_url: String,
}

#[derive(Serialize, Deserialize, Debug, Default)]
pub struct Repository {
    pub name: String,
    pub url: String,
    pub description: String,
    pub homepage: String,
    pub git_http_url: String,
    pub git_ssh_url: String,
    pub visibility_level: u64,
}

#[derive(Serialize, Deserialize, Debug, Default)]
pub struct Commit {
    pub id: String,
    pub message: String,
    pub title: String,
    pub timestamp: String,
    pub url: String,
    pub author: Author,
    pub added: Vec<String>,
    pub modified: Vec<String>,
    pub removed: Vec<String>,
}

#[derive(Serialize, Deserialize, Debug, Default)]
pub struct Author {
    pub name: String,
    pub email: String,
}
