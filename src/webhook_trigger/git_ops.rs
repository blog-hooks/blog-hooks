use super::PushError;
use git2::{
    build::CheckoutBuilder, AnnotatedCommit, AutotagOption::All, Cred, FetchOptions,
    RemoteCallbacks, Repository, Tree,
};
use log::{error, info};
use std::{
    env,
    path::{Path, PathBuf},
};

pub fn update_the_repo(blog_location: &PathBuf) -> Result<(), PushError> {
    info!("Updating repo at: {}", blog_location.to_string_lossy());
    let repo = get_repo(blog_location).map_err(|e| {
        error!("Failed to pull latest changes: {}", e);
        PushError::ErrorPullingLatest
    })?;
    do_fetch(&repo).and_then(|_| do_merge(repo))
}

fn get_repo(blog_location: &PathBuf) -> Result<Repository, PushError> {
    let repo = Repository::open(blog_location);
    if repo.is_err() {
        return Err(PushError::InvalidGitRepo);
    }

    Ok(repo.unwrap())
}

fn do_merge(repo: Repository) -> Result<(), PushError> {
    repo.find_reference("FETCH_HEAD")
        .map_err(|_| PushError::ErrorPullingLatest)
        .and_then(|fetch_head| {
            repo.reference_to_annotated_commit(&fetch_head)
                .map_err(|_| PushError::ErrorPullingLatest)
        })
        .and_then(|commit| {
            repo.merge_analysis(&[&commit])
                .map(|merge_analysis| (merge_analysis, commit))
                .map_err(|_| PushError::ErrorPullingLatest)
        })
        .and_then(|(analysis, commit)| {
            if analysis.0.is_fast_forward() {
                do_fast_foward(&repo, commit)
            } else {
                do_normal_merge(&repo, commit)
            }
        })
}

fn do_fast_foward(repo: &Repository, commit: AnnotatedCommit) -> Result<(), PushError> {
    let name = match &commit.refname() {
        Some(s) => s.to_string(),
        None => String::from_utf8_lossy(commit.refname_bytes()).to_string(),
    };

    repo.find_reference("refs/heads/master")
        .map_err(|_| PushError::UnableToUpdateSite)
        .and_then(|mut reference| {
            reference
                .set_target(commit.id(), "Fast forwarding master")
                .map_err(|_| PushError::UnableToUpdateSite)
        })
        .and_then(|_| {
            repo.set_head(&name)
                .map_err(|_| PushError::ErrorPullingLatest)
        })
        .and_then(|_| {
            repo.checkout_head(Some(CheckoutBuilder::default().force()))
                .map_err(|_| PushError::ErrorPullingLatest)
        })
}

fn do_normal_merge(repo: &Repository, commit: AnnotatedCommit) -> Result<(), PushError> {
    let head_commit = repo
        .reference_to_annotated_commit(&repo.head().map_err(|_| PushError::ErrorPullingLatest)?)
        .map_err(|_| PushError::ErrorPullingLatest)?;

    let local_tree = find_tree(repo, &commit)?;
    let remote_tree = find_tree(repo, &head_commit)?;
    let ancestor = find_ancestor_tree(repo, &commit, &head_commit)?;

    let mut merged_trees = repo
        .merge_trees(&ancestor, &local_tree, &remote_tree, None)
        .map_err(|_| PushError::ErrorPullingLatest)?;

    if merged_trees.has_conflicts() {
        repo.checkout_index(Some(&mut merged_trees), None)
            .map_err(|_| PushError::ErrorPullingLatest)?;
        return Ok(());
    }

    let result_tree = repo
        .find_tree(
            merged_trees
                .write_tree_to(repo)
                .map_err(|_| PushError::ErrorPullingLatest)?,
        )
        .map_err(|_| PushError::ErrorPullingLatest)?;
    // now create the merge commit

    let sig = repo
        .signature()
        .map_err(|_| PushError::ErrorPullingLatest)?;

    let local_commit = repo
        .find_commit(commit.id())
        .map_err(|_| PushError::ErrorPullingLatest)?;
    let remote_commit = repo
        .find_commit(head_commit.id())
        .map_err(|_| PushError::ErrorPullingLatest)?;

    repo.commit(
        Some("HEAD"),
        &sig,
        &sig,
        "Pulled latest",
        &result_tree,
        &[&local_commit, &remote_commit],
    )
    .map_err(|_| PushError::ErrorPullingLatest)
    .and_then(|_| {
        repo.checkout_head(None)
            .map_err(|_| PushError::ErrorPullingLatest)
    })
}

fn find_ancestor_tree<'a>(
    repo: &'a Repository,
    local_commit: &AnnotatedCommit,
    remote_commit: &AnnotatedCommit,
) -> Result<Tree<'a>, PushError> {
    repo.find_commit(
        repo.merge_base(local_commit.id(), remote_commit.id())
            .map_err(|_| PushError::ErrorPullingLatest)?,
    )
    .map_err(|_| PushError::ErrorPullingLatest)?
    .tree()
    .map_err(|_| PushError::ErrorPullingLatest)
}

fn find_tree<'a>(repo: &'a Repository, commit: &AnnotatedCommit) -> Result<Tree<'a>, PushError> {
    repo.find_commit(commit.id())
        .map_err(|_| PushError::ErrorPullingLatest)
        .and_then(|found_commit| {
            found_commit
                .tree()
                .map_err(|_| PushError::ErrorPullingLatest)
        })
}

fn do_fetch(repo: &Repository) -> Result<(), PushError> {
    let mut callbacks = RemoteCallbacks::new();
    callbacks.credentials(|_url, username_from_url, _allowed_types| {
        Cred::ssh_key(
            username_from_url.unwrap(),
            None,
            Path::new(&format!(
                "{}/.ssh/id_rsa",
                env::var("$HOME").unwrap_or_else(|_| "/root".to_owned())
            )),
            None,
        )
    });
    let mut remote = repo.find_remote("origin").expect("unable to find remote");

    let mut fetch_options = FetchOptions::new();
    fetch_options.remote_callbacks(callbacks);

    fetch_options.download_tags(All);
    let fetch = remote.fetch(&["master"], Some(&mut fetch_options), None);

    if fetch.is_err() {
        error!("Trouble pulling latest changes: {}", fetch.unwrap_err());
        Err(PushError::ErrorPullingLatest)
    } else {
        Ok(())
    }
}
