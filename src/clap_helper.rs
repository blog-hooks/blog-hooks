use clap::{App as Clapp, Arg, ArgMatches};
use std::{path::PathBuf, process::exit};

use hugo_checker::get_hugo;

mod hugo_checker;

pub fn clap_up() -> Option<PathBuf> {
    let matches = init_clap();

    if !matches.is_present("hugo_location") {
        return None;
    }

    let hugo = match get_hugo(matches.value_of("hugo_location").unwrap()) {
        Ok(p) => p,
        Err(e) => {
            eprintln!("Bad Hugo file: {:?} - exiting", e);
            exit(1);
        }
    };

    Some(hugo)
}

fn init_clap<'a>() -> ArgMatches<'a> {
    Clapp::new("Bloghook updater")
        .version("0.3.0")
        .author("Darrien G. <me@darrien.dev>")
        .about("Blog auto updater - updates a blog from gitlab webhook and stores in location for nginx")
        .arg(Arg::with_name("hugo_location").short("l").long("hugo-location").help("location of hugo binary").takes_value(true).required(false)
        )
        .get_matches()
}
