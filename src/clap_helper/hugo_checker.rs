use std::{fs::File, os::unix::fs::PermissionsExt, path::PathBuf};
use thiserror::Error;

#[derive(Error, Debug)]
pub enum ExeError {
    #[error("Hugo binary does not exist at specified location")]
    DoesNotExist,
    #[error("Hugo binary must be a regular file")]
    InvalidFileType,
    #[error("Hugo binary  not executable")]
    BadPermissions,
}

pub fn get_hugo(hugo_path: &str) -> Result<PathBuf, ExeError> {
    let mut path = PathBuf::new();
    path.push(hugo_path);

    if !hugo_exists(&path) {
        Err(ExeError::DoesNotExist)
    } else if !hugo_is_file(&path) {
        Err(ExeError::InvalidFileType)
    } else if !hugo_is_executable(&path) {
        Err(ExeError::BadPermissions)
    } else {
        Ok(path)
    }
}

fn hugo_exists(path: &PathBuf) -> bool {
    path.exists()
}

fn hugo_is_file(path: &PathBuf) -> bool {
    path.is_file()
}

fn hugo_is_executable(path: &PathBuf) -> bool {
    let f = File::open(path).unwrap();
    let metadata = f.metadata().unwrap();
    metadata.permissions().mode() >= 0o100700
}
