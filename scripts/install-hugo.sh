#!/usr/bin/env bash

set -eou pipefail

FILE_LOCATION="/tmp/hugo.deb"
URI=https://github.com/gohugoio/hugo/releases/download/v0.89.2/hugo_extended_0.89.2_Linux-64bit.deb

wget -O "$FILE_LOCATION" "$URI"
dpkg -i "$FILE_LOCATION"
apt-get install -f
rm "$FILE_LOCATION"
