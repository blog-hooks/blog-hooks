FROM rust:1.56.1-buster AS BUILD

RUN mkdir /build

WORKDIR /build

COPY src src
COPY Cargo.toml Cargo.lock ./

RUN cargo build --release && strip ./target/release/blog-hooks

FROM ubuntu:20.04

USER root

RUN apt-get update -y && apt-get install -y wget dumb-init

COPY /scripts/install-hugo.sh /tmp/install-hugo.sh

RUN /tmp/install-hugo.sh

RUN useradd -ms /bin/bash hooks

USER hooks

COPY --from=BUILD /build/target/release/blog-hooks ./blog-hooks

ENV RUST_LOG=info

ENTRYPOINT ["dumb-init", "--", "./blog-hooks", "--hugo-location", "/usr/local/bin/hugo"]
