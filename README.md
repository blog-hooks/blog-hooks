# Blog Hooks - The low infra blog updating solution

You know the drill, you want to run a custom site and you're using Hugo because
HTML is bad, and modern webdev is worse.

So you write up your first post locally and have a great time with it. You spin
up a server on a VPS or one of your local servers, `git clone` your site and run
hugo, show it to the world, and life is good.

A few days later and you go off to write your second post. Ready to go, you go
ahead, git push it, and remember you have to go to your site, delete the old
`public` folder, run `hugo`, delete the old folder under nginx, copy everything
over and get annoyed.

You do it, but then realize you have a typo and do it again, and again, and
again, and again...

## Alternatives

So you start looking into maybe not running your own infra for your small blog.

Services like netlify exist, but are stored entirely on infra you don't control.

GitLab and GitHub both offer services to deploy your hugo site on infra control,
but the setup is a pain and GitHub is owned by Microsoft. Ideally we stop giving
them so much data.  Even if you are fine giving them data, you must speak
terrible yaml incantations to make it work.  You just want to write your blog
and grill for
god's sake!

## It doesn't have to be this way

In comes `blog-hooks`. For the person who already has their own server and has
nginx or apache set up, or wants to run their own infra agnostic of a provider,
`blog-hooks` gets a webhook (right now just from gitlab), updates your site the
right way every time, and lets you get back to grilling.

Look at how easy this guy is:

```bash
$ blog-hooks --help
Bloghook updater 0.3.0
Darrien G. <me@darrien.dev>
Blog auto updater - updates a blog from gitlab webhook and stores in location for nginx

USAGE:
    blog-hooks [OPTIONS]

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
    -l, --hugo-location <hugo_location>    location of hugo binary
```

Ahh, look at how clean. Only one command line argument, and it's optional! Only
required if you plan on using Hugo built sites.

The secret to it being so simple is a little config is hidden in each repo we
want to update. That too is only 2 lines though!

## How does it work?

The blog hooks updater can handle any number of sites without being configured
ahead of time to know about them. How is this possible? Through the magic of
path params of course!

Here's what a configured webhook might look like:

`my-webhook.example.com/site-webhook/opt-my_site`

Where my-webhook.example.com is where your site is located. `site-webhook` is
the subpath we receive requests on, and finally `opt-blog` is a dash separated
**absolute** path to where your site is located.

At that absolute path is a directory that contains one of two config files named
aptly: `config.toml`.  They may not be named anything else.

`blog-hooks` currently supports 2 kind of sites, simple (e.g. everything is in a
git repo and it's all static, no building), and hugo (you require a run of
`hugo` on your site after the git pull, and your `public` folder saved to where
you host your data).

If you have a `simple` site your config looks like this:

```toml
[simple_site]
site_path = "/opt/main_site/site_git_dir"
```

Yeah that's it. `site_path` is the path to where your git repo is and it will
pull it. That's really it. Note that the path **MUST** be absolute.


If you have a `hugo` site, your config will look like this:

```toml
[hugo_site]
unprocessed_site_path = "/opt/dev_blog/raw"
processed_site_path = "/opt/dev_blog/processed"
```

Only one extra line. The first line says where the git repo for your hugo site
is, and the second line says where you'd like to copy your `public` folder to
once everything is done processing.

And that's it!

## Binaries

We support x86 Linux only right now.

We build using rust though, so making your own binary is as easy as a:

```
cargo build --release  # binary is ./target/release/blog-hooks
```

An official Docker image is also provided
[here](https://registry.hub.docker.com/r/darrieng/gitlab-hooks).

A systemd service is located in the `./startup` folder for extra convenience.

## FAQ

Q: Does this mean my folders can't have dashes (`-`) in them if I use blog-hooks?

A: That's correct

***

Q: Why won't my hugo site update? I'm getting an angry error.

A: Make sure you supplied the `hugo-location` argument. If you didn't, it won't
update! Whether you downloaded it manually or it's in your path, it has to be
specified.

***

Q: Does `blog-hooks` leak information to my callers? e.g. where files and folders
are located?

A: `blog-hooks` will not naively respond with good information in the response
message. e.g. if the path to your config file is bad, or the path to your
website is bad, it will not tell the caller what the issue is. The only bits of
information it will tell the caller is: if the path to the blog is wrong or if
the config file is misconfigured.

* **NOTE:** This does mean a malicious user who knows about this service and your
  usage of it can glean a little bit of information about your system. However
  the most they will know is whether you have a blog in a location or not, and
  whether your config file is there or not.

***

Q: Well since the message responses are bad, does this mean I can't debug why my
service is having trouble being updated by `blog-hooks`?

A: You can't by response, at least you won't get much good information. The
`blog-hooks` logs will provide good information for debugging. Just make sure to
run with env var: `RUST_LOG=debug` for maximum info.

***
